// 01 a
    function sampleA() {
        function a() {
            alert("Привет мир")
        }

        a();
    }
//02 cube
function sampleCube1() {
    function cube(b, n) {
        let c = b;
        for (let i = 1; i < n; i++) {
            c *= b;
        }
        return c;
    }
    let b = prompt("Введите число");
    let n = prompt("Введите степень");
    alert(cube(b, n));
}
//02 cube(2)
function sampleCube2() {
    function cubes(a) {
        return a**3;
    }
    alert(cubes(2));
}
//03 avg2
function sampleAvg2(){
    function avg2(a,e){
        return (a+e)/2;
    }
    let a = +prompt("Введите число");
    let e = +prompt("Введите число");
    alert( avg2(a,e));
}
//04 sum3
function sampleSum3() {
    function sum3(a,b,c){
        a = a || 0;
        b = b || 0;
        c = c || 0;
        return a + b + c;
    }
    sum3(5,10);
}

//05 intRandom
function sampleIntRandom() {
    function intRandom(min, max) {
        let random = min - 0.5 + Math.random() * (max - min + 1);
        random = Math.round(random);
        return random;
    }
    alert(intRandom(5, 10) );
    alert(intRandom(10));
}
//06greetAll
function sampleGreetAll() {
    function greetAll () {
        let string = "";
        for(let name of arguments) {
            string += `${name},`;
        }
        alert(`"Hello ${string.slice(0,string.length - 2)}!"`);
    }
    greetAll("Дима", "Ваня", "Саша");
}


//07sumAll
function sampleSumAll() {
    function sumAll(...arr)
     {
        let result = 0;
        for(let x of arr) {
            result += x;
        }
        alert(result);
    }
    sumAll(1,2,3,4,5,6,7);
}

let sample = prompt("Введите название задания","");
switch (sample.toLowerCase()) {
    case "a":         sampleA();
        break;
    case "cube":      sampleCube1();
        break;
    case "cube2":     sampleCube2();
        break;
    case "avg2":      sampleAvg2();
        break;
    case "sum3":      sampleSum3();
        break;
    case "intRandom": sampleIntRandom();
        break;
    case "greetAll":  sampleGreetAll();
        break;
    case "SumAll":    sampleSumAll();
}


let homeW = new Map([
    ["a", sampleA],
    ["cube", sampleCube1()],
    ["cube2", sampleCube2()],
    ["avg2", sampleAvg2()],
    ["sum3", sampleSum3()],
    ["intRandom", sampleIntRandom()],
    ["greetAll", sampleGreetAll()],
    ["sumAll", sampleSumAll()]
]);
console.log(homeW);








